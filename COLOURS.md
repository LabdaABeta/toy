## Test Driven Development

This project is intended to be a sample of how a test-driven-development (TDD)
system can be used to develop a programming language. It also serves as a basic
test-bed for my personal flavour of TDD.

### Colours

Each commit must have a colour, with each colour assigned to a basic step in the
TDD methodology.

**Red** commits indicate new features added via tests. These tests should fail.
Their eventual "fix" would indicate the feature is successfully added (or bug
successfully fixed). **Magenta** commits are used for refactoring of tests and
adding new test infrastructure.

**Green** commits indicate a "fix" to a broken test (or implementation of
missing feature). These must always make at least one previously failing test
pass. **Yellow** commits can be used in the interim for progress towards a "fix"
but not a full fix yet.

**Blue** commits indicate a refactoring that doesn't break any tests. Code
should be refactored between each round of feature addition to ensure
maintainability. **Cyan** commits can be used in the interim for refactoring
that temporarily causes some tests to fail, these should be followed by
**green** commits re-fixing those tests eventually.

**White** commits are used for updating documentation in the end product.
**Grey** commits can be used for updating internal documentation that is not
typically visible in the end product (e.g. code comments).

**Black** commits are the catch-all "other" commits.

The result of this should be that in general commits work their way around the
colour wheel by hue (though not necessarily monotonically). The greyscale
commits can appear at any point, while the coloured ones tend to move in
increasing hue (rygcbmr). Most cycles of colour are started by white commits
documenting the desired features and terminated by more white commits
documenting that said features are completed.

## Colour your commits

Commits will only be accepted if they are properly coloured. The commit message
must start with the lower-case letter corresponding to the commit colour
followed by a colon. The colours are:

 - r: Red
 - y: Yellow
 - g: Green
 - c: Cyan
 - b: Blue
 - m: Magenta
 - w: White
 - k: Black
 - e: Grey
