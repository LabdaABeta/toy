# Toy Language

Like joy, but typed.

## Syntax

### Strings

Strings are continuously quoted with:

    "'`

as the possible quotes. A pair of repeated quote marks denotes a single empty
string. An odd number of repeated quote marks denotes a string containing the
continuous bytes delimited by the same repeated quote marks.

Examples:

    "" """" '''''' ``
    "Those were all empty strings!"
    'The three quote types are interchangeable'
    `Typically you would want to be consistent`
    """Repeat quotes an odd number of times to embed "'` quotes"""

### Numbers

Numbers can be in any base from unary to base 36 using digits from the alphabet:

    0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ

The digit alphabet is case insensitive and underscores `_` can be used to
indicate grouping. If a base is specified, it is placed before a number sign `#`
before the rest of the number. The base may be specified as either a single
base-36 digit, or by the empty string to denote base 36.

Numbers may have a radix point.

In general, numbers follow this grammar:

    number ::= [base #] integer [.integer]
    base ::= digit
    integer ::= digit {[underline] digit}

Note: These numbers are similar to Ada's numeric literals, but the exponent is
in the same base as the rest of the number.

### Identifiers

Identifiers are either symbols or words. Words start with letters or underscore
and consist of contiguous alphanumeric characters. Symbols consist of contiguous
symbolic characters. Note that `._$` are valid identifier characters.

### Blocks - NOT IMPLEMENTED

Blocks of code can be written by surrounding them with square brackets: `[ ]`

The code between the square brackets is seen as a literal value.

## Syntactic Sugar

### Arguments - NOT IMPLEMENTED

Code between round brackets `( )` is prepended to the previous word. This can
make arguments look like they are applied in postfix notation despite being
prefixed.

Examples:

    foo ( bar ) -> bar foo
    foo ( bar baz ) -> bar baz foo
    foo ( bar ) baz -> bar foo baz
    foo ( bar ) ( baz ) -> bar foo ( baz ) -> bar baz foo

### Blocks - NOT IMPLEMENTED

Code between braces is prepended as a block to the previous word. This can make
block arguments look like they are applied in postfix notation despite being
prefixed.

Examples:

    foo { baz } -> [ baz ] foo
    foo { bar baz } -> [ bar baz ] foo
    foo ( bar ) { baz } -> bar foo { baz } -> bar [ baz ] foo
    foo { baz } ( bar ) -> [ baz ] foo ( bar ) -> [ baz ] bar foo

## Typing - NOT DOCUMENTED


