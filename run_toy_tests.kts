#!/usr/bin/kscript

@file:MavenRepository("jitpack", "https://jitpack.io")

@file:DependsOn("cc.ekblad:4koma:1.0.1")
@file:DependsOn("com.github.ajalt.clikt:clikt-jvm:3.4.0")
@file:DependsOn("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")

import kotlinx.coroutines.flow.*
import kotlinx.coroutines.*

import java.io.File
import java.nio.file.Path

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.types.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.arguments.*

import cc.ekblad.toml.*

@file:Include("./testsrc/tap.kt")
@file:Include("./testsrc/commands.kt")
@file:Include("./testsrc/testsuite.kt")

suspend fun TAPScope.runTestExec(executable: String, exec: TestSuite.Test.Exec, dir: File) {
    try {
        val result = runCommand(executable, *(exec.options?.toTypedArray() ?: arrayOf()))

        if (result.code != exec.exit) {
            fail("Result codes for execution don't match.")
        } else {
            if (result.output != exec.output) {
                fail("Result output doesn't match.")
            } else {
                pass()
            }
        }
    } catch (e: java.io.IOException) {
        fail("$executable - failed to run")
    }
}

suspend fun TAPScope.runTestCase(toyExec: File, test: TestSuite.Test, dir: File) {
    val result = runCommand(
        toyExec.absolutePath,
        *(test.options?.toTypedArray() ?: arrayOf()),
        "-o", test.opath(),
        test.input,
        workingDir = dir
    )

    if (test.compiles == false) {
        if (result.code != 0)
            pass("${test.description} - failed to compile, as expected")
        else
            fail("${test.description} - compiled, when should not have.")
    } else {
        if (result.code != 0)
            fail("${test.description} - failed to compile")
        else if (!test.ofile(dir).exists())
            fail("${test.description} - failed to produce output")
        else {
            if (test.execs != null && test.execs?.isNotEmpty() == true) {
                sub("Executions for ${test.description}") {
                    for (exec in test.execs ?: emptyList())
                        runTestExec(test.opath(), exec, dir)
                }
            } else if (test.diff != null) {
                if (test.diff.matchesIn(dir, test.ofile(dir).readText()))
                    pass("${test.description} - content matches")
                else
                    fail("${test.description} - content differs")
            } else {
                pass("${test.description} - compilation successful")
            }
        }
    }
}

suspend fun TAPScope.runTestSuite(toyExec: File, file: File, dir: File) {
    val suite = suiteMapper.decode<TestSuite>(file.inputStream())

    sub("${suite.name} in $file") {
        for (test in suite.tests) {
            runTestCase(toyExec, test, dir)
            test.ofile(dir).delete()
        }
    }
}

class RunToyTests : CliktCommand() {
    val tests : List<Path> by argument().path(mustExist = true).multiple()
    val toyExec : File by option("-t", "--toy", help="Override the toy compiler under test").file().default(File("toy"))

    override fun run() {
        val testList = if (tests.isNotEmpty()) tests else listOf(File("tests"))
        runBlocking {
            tap {
                for (test in testList) {
                    val result = runCommand("find", test.toString(), "-name", "test_*.toml")
                    val suites = result.output.lines()

                    for (suite in suites.dropLast(1)) {
                        val suiteFile = File(suite)
                        val suiteDir = suiteFile.getParentFile()
                        runTestSuite(toyExec, suiteFile, suiteDir)
                    }
                }
            }.collect { println(it) }
        }
    }
}

RunToyTests().main(args)
