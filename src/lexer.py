"""
Toy lexer.
"""
from typing import Generator, Iterable, Optional
import itertools

from source import SourceString, load


LETTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
IDENTIFIER_START_CHARS = LETTERS + '._$#'
DIGITS = '0123456789'
IDENTIFIER_CHARS = IDENTIFIER_START_CHARS + DIGITS
WHITESPACE = ' \t\n\r'
QUOTE_MARKS = '\'"`'
SINGLE_MARKS = '({[]})'
# All other characters are symbols


class LexState:
    """ Represents the state of a lexer. """
    def __init__(self):
        self._delim = ''
        self._close = ''
        self._token = None
        self._start = False

    def __str__(self):
        return f'{self._delim} - {self._close} - {self._token} - {self._start}'

    def _update_raw_continue(self, character):
        result = None

        # This processes "forwarded" singleton results in _token
        if self._token.content in SINGLE_MARKS:
            result = self._token
            self._token = None
            self._token = self._update_raw(character)
            return result

        if character.content == '\\':
            result = self._token
            self._delim = character.content
            self._start = True

        elif character.content in QUOTE_MARKS:
            result = self._token
            self._delim = character.content
            self._token = character
            self._start = True

        elif character.content in SINGLE_MARKS:
            result = self._token
            self._token = character

        elif character.content not in WHITESPACE:
            char_ident = (character.content in IDENTIFIER_CHARS)
            tok_ident = (self._token.content[0] in IDENTIFIER_CHARS)

            if char_ident == tok_ident:
                self._token.content += character.content
            else:
                result = self._token
                self._token = None
                self.update(character)

        else:
            result = self._token
            self._token = None

        return result

    def _update_raw_empty(self, character):
        if character.content == '\\':
            self._delim = character.content
            self._start = True

        elif character.content in QUOTE_MARKS:
            self._delim = character.content
            self._token = character
            self._start = True

        elif character.content in SINGLE_MARKS:
            return character

        elif character.content not in WHITESPACE:
            self._token = character

        return None

    def _update_raw(self, character):
        if self._token:
            return self._update_raw_continue(character)
        return self._update_raw_empty(character)

    def _update_line_comment(self, character):
        if self._start:
            if character.content == '\\':
                self._delim += '\\'
            else:
                self._start = False
        else:
            if character.content == '\n':
                self._delim = ''
                self._start = False

    def _update_block_comment(self, character):
        if self._start:
            if character.content == '\\':
                self._delim += '\\'
            else:
                self._start = False
        else:
            if character.content == '\\':
                self._close += '\\'
                if self._close == self._delim:
                    self._delim = ''
                    self._close = ''
                    self._start = False

            else:
                self._close = ''

    def _update_string(self, character):
        result = None
        self._token.content += character.content

        if self._start:
            if character.content == self._delim[0]:
                self._delim += character.content
            else:
                self._start = False
                if len(self._delim) % 2 == 0:
                    result = SourceString(
                            content=self._token.content[:-1],
                            location=self._token.location)
                    self._token = None
                    self._delim = ''
                    self._close = ''
                    self._start = False
                    self.update(character)
        else:
            if character.content == self._delim[0]:
                self._close += character.content
                if self._close == self._delim:
                    result = self._token
                    self._token = None
                    self._delim = ''
                    self._close = ''
                    self._start = False
            else:
                self._close = ''

        return result

    def update(self, character: SourceString) -> Optional[SourceString]:
        """
        Process the next character of stream and possibly return a lexeme.
        """
        if not self._delim:
            return self._update_raw(character)
        if self._delim == '\\':
            self._update_line_comment(character)
            return None
        if self._delim[0] == '\\':
            self._update_block_comment(character)
            return None
        return self._update_string(character)

    def end(self) -> Optional[SourceString]:
        """ Process the end of stream and possibly return a final lexeme. """
        return self._token


def lex(src: Iterable[SourceString]) -> Generator[SourceString, None, None]:
    """
    A generator that tokenizes the data in the input source.
    """
    state = LexState()

    for character in src:
        result = state.update(character)
        if result is not None:
            yield result

    result = state.end()
    if result is not None:
        yield result


def lex2(src: Iterable[str]) -> Generator[str, None, None]:
    """
    A generator that tokenizes the data in the input source.
    A marginally more functional implementation.
    """
    items = iter(itertools.chain.from_iterable(src))

    def _block_comment():
        size = 2
        while next(items) == '\\':
            size += 1

        close = 0
        while close < size:
            if next(items) == '\\':
                close += 1
            else:
                close = 0

    def _line_comment():
        first = next(items)
        if first == '\\':
            _block_comment()
        elif first != '\n':
            while next(items) != '\n':
                pass

    def _quoted(start):
        token = start
        size = 1
        for item in items:
            if item == start:
                size += 1
                token += item
            else:
                if size % 2 == 0:
                    return (token, item)
                token += item
                break

        close = 0
        while close < size:
            token += next(items)
            if token[-1] == start:
                close += 1
            else:
                close = 0

        return (token, None)

    def _bare(start):
        token = start

        for item in items:
            if (item in IDENTIFIER_CHARS) == (start in IDENTIFIER_CHARS):
                token += item
            else:
                return (token, item)

        return (token, None)

    peek = None
    try:
        while True:
            if peek:
                item = peek
                peek = None
            else:
                item = next(items)

            if item == '\\':
                _line_comment()
            elif item in QUOTE_MARKS:
                (quotation, peek) = _quoted(item)
                yield quotation
            elif item not in WHITESPACE:
                (word, peek) = _bare(item)
                yield word
    except StopIteration:
        return


if __name__ == '__main__':
    import sys
    for lexeme in lex(load(sys.stdin, '-')):
        print('<' + ''.join(lexeme) + '>')
