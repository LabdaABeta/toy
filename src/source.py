"""
Toy source file mapping.
"""

from typing import Generator, Iterable
from dataclasses import dataclass


@dataclass
class SourceLocation:
    """ Represents a position within an input source. """
    file: str
    row: int
    col: int


@dataclass
class SourceString:
    """ Represents a string within an input source. """
    content: str
    location: SourceLocation


def load(src: Iterable[str], name: str) -> Generator[SourceString, None, None]:
    """
    A generator that localizes the data in the input source.
    """
    row: int = 1
    col: int = 1

    for line in src:
        for character in line:
            yield SourceString(character, SourceLocation(name, row, col))

            if character == '\n':
                row += 1
                col = 1
            else:
                col += 1
