"""
Toy tokenizer.
"""
from enum import Enum, auto
from typing import Generator, Iterable
from dataclasses import dataclass

import re

from source import SourceString, SourceLocation
from lexer import IDENTIFIER_CHARS, QUOTE_MARKS


class ToyTokenKind(Enum):
    """ Represents the possible kinds of tokens in Toy. """
    IDENTIFIER = auto()  # value -> str
    OPERATOR = auto()  # value -> str
    NUMBER = auto()  # value -> float | int
    STRING = auto()  # value -> str
    OPEN_BRACKET = auto()  # [
    CLOSE_BRACKET = auto()  # ]
    OPEN_BRACE = auto()  # {
    CLOSE_BRACE = auto()  # }
    OPEN_PAREN = auto()  # (
    CLOSE_PAREN = auto()  # )


@dataclass
class ToyToken:
    """ Represents a token and all information it may represent. """
    location: SourceLocation
    lexeme: str
    kind: ToyTokenKind
    value: str | float | int | None


def normalize_identifier(raw: str) -> str:
    """
    Normalizes the identifier. In future multiple identifiers may be similar.
    """
    return raw


def normalize_operator(raw: str) -> str:
    """
    Normalizes the operator. In future multiple operators may be similar.
    """
    return raw


def strip_quote_marks(raw: str) -> str:
    """ Removes quotation marks from around a string. """
    quotesz = 0
    while raw[quotesz] == raw[0]:
        quotesz += 1
        if quotesz == len(raw):
            return ""

    return raw[quotesz:-quotesz]


def numeric_value(raw: str) -> float | int | None:
    """
    Converts a numeric literal into a value. Returns None if the literal is not
    numeric.
    """
    regex = r"([0-9A-Z]?#)?([0-9A-Z]+)(\.[0-9A-Z]+)?"
    parts = re.search(regex, raw, re.IGNORECASE)

    if not parts:
        return None

    (basepart, intpart, decpart) = parts.groups()

    if basepart:
        digitstr = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ#'
        base = digitstr.index(basepart[0].upper())
    else:
        base = 10

    if base < 2 or base > 36:
        return None

    validchars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'[:base]
    alldigits = intpart
    if decpart is not None:
        alldigits += decpart[1:]
    if any(x not in validchars for x in alldigits.upper()):
        return None

    result = int(intpart, base)

    if decpart:
        dec = float(int(decpart[1:], base))
        dec /= base ** len(decpart[1:])

        result = float(result) + dec

    return result


def tokenize(src: Iterable[SourceString]) -> Generator[ToyToken, None, None]:
    """
    A generator that analyzes the tokens in the input stream.
    """
    for token in src:
        lexeme = token.content
        value = None

        if lexeme[0] == '[':
            kind = ToyTokenKind.OPEN_BRACKET
        elif lexeme[0] == ']':
            kind = ToyTokenKind.CLOSE_BRACKET
        elif lexeme[0] == '{':
            kind = ToyTokenKind.OPEN_BRACE
        elif lexeme[0] == '}':
            kind = ToyTokenKind.CLOSE_BRACE
        elif lexeme[0] == '(':
            kind = ToyTokenKind.OPEN_PAREN
        elif lexeme[0] == ')':
            kind = ToyTokenKind.CLOSE_PAREN
        elif lexeme[0] in IDENTIFIER_CHARS:
            value = numeric_value(lexeme)
            if value is None:
                kind = ToyTokenKind.IDENTIFIER
                value = normalize_identifier(lexeme)
            else:
                kind = ToyTokenKind.NUMBER
        elif lexeme[0] in QUOTE_MARKS:
            kind = ToyTokenKind.STRING
            value = strip_quote_marks(lexeme)
        else:
            kind = ToyTokenKind.OPERATOR
            value = normalize_operator(lexeme)

        yield ToyToken(
            lexeme=lexeme,
            kind=kind,
            value=value,
            location=token.location
        )
