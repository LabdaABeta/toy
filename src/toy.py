#!/usr/bin/env python3

"""
Toy compiler.
"""
from typing import TextIO

import click

from source import load
from lexer import lex
from tokenizer import tokenize
# import tokenizer
# import parser


@click.command()
@click.option(
    '-o', '--output',
    metavar='FILE', default='a.out', show_default=True, type=click.File('w')
)
@click.option('--just-lex', is_flag=True)
@click.option('--just-tok', is_flag=True)
@click.argument('input', default='-', type=click.File('r'))
def main(output: TextIO, input: TextIO, just_lex: bool, just_tok: bool):
    # pylint: disable=W0622
    """
    Basic toy compiler (to be replaced with a bootstrapped compiler later)
    """
    if just_lex:
        for lexeme in lex(load(input, input.name)):
            output.write('<' + ''.join(lexeme.content) + '>\n')
        output.write('')  # ensure file created on empty result

    elif just_tok:
        for token in tokenize(lex(load(input, input.name))):
            output.write(f'{token.kind}: {token.value}\n')
        output.write('')  # ensure file created on empty result


if __name__ == '__main__':
    main()  # pylint: disable=E1120
