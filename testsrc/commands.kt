data class CommandResult(
    val output : String,
    val error : String,
    val code : Int
)

class Command(val executable : String, val args : Array<String>) {
    fun run(workingDir : File = File(".")) : CommandResult {
        // System.err.println("Executing: $executable with ${args.joinToString()} in $workingDir...")
        val process = ProcessBuilder(listOf(executable) + args)
            .directory(workingDir)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        val code = process.waitFor()
        val output = process.inputStream.bufferedReader().readText()
        val error = process.errorStream.bufferedReader().readText()

        return CommandResult(output, error, code)
    }
}

fun runCommand(
    executable : String,
    vararg args : String,
    workingDir : File = File(".")
) : CommandResult {
    try {
        return Command(executable, arrayOf(*args)).run(workingDir)
    } catch (e: java.io.IOException) {
        return CommandResult("", "", -1)
    }
}

