class TAPScope(private val scope: FlowCollector<String>, private val indent: Int = 0) {
    private val i = "    ".repeat(indent)
    private var number = 0
    private var failed = false
    private var finished = false

    public suspend fun pass(description: String? = null) {
        if (description != null)
            scope.emit("${i}ok ${++number} - $description")
        else
            scope.emit("${i}ok ${++number}")
    }

    public suspend fun fail(description: String? = null) {
        failed = true
        if (description != null)
            scope.emit("${i}not ok ${++number} - $description")
        else
            scope.emit("${i}not ok ${++number}")
    }

    public suspend fun done(reason: String? = null) {
        finished = true
        if (reason != null)
            scope.emit("${i}1..$number # $reason")
        else
            scope.emit("${i}1..$number")
    }

    public suspend fun sub(name: String? = null, fill: suspend TAPScope.() -> Unit) {
        val subscope = TAPScope(scope, indent + 1)

        if (name != null)
            scope.emit("${i}# Subtest: $name")
        else
            scope.emit("${i}# Subtest")

        subscope.fill()
        subscope.close()
        if (subscope.failed)
            fail()
        else
            pass()
    }

    suspend fun close() {
        if (!finished)
            done()
    }
}

fun tap(fill: suspend TAPScope.() -> Unit): Flow<String> = flow {
    val scope = TAPScope(this)

    emit("TAP version 14")
    scope.fill()
    scope.close()
}
