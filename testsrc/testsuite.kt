data class TestSuite(
    val name : String,
    val tests : List<Test>
) {
    data class Test(
        val name : String,
        val input : String,
        val output : String?,
        val options : List<String>?,
        val execs : List<Exec>?,
        val diff : Diff?,
        val compiles : Boolean?
    ) {
        data class Exec(
            val options : List<String>?,
            val output : String,
            val exit : Int
        )

        data class Diff(
            val expected : String?,
            val expectedFile : String?
        ) {
            fun matchesIn(dir: File, text: String) : Boolean =
                if (expected != null)
                    text == expected
                else
                    text == File("$dir/" + expectedFile!!).readText()
        }

        val description : String get() = "$name ($input)"
        fun opath(dir: File? = null): String =
            (if (dir != null) "$dir/" else "") + (output ?: "a.out")
        fun ofile(dir: File? = null): File = File(opath(dir))
    }
}

val suiteMapper = tomlMapper {
    mapping<TestSuite>("test" to "tests")
    mapping<TestSuite.Test>("exec" to "execs")
    mapping<TestSuite.Test.Diff>("expected-file" to "expectedFile")
}

